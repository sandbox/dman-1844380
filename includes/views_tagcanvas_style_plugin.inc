<?php

/**
  * Implementation of views_plugin_style().
  */
class views_tagcanvas_style_plugin extends views_plugin_style {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options += array(
      'freezeActive' => array('default' => TRUE),
        /*
            textColour: '#0077C0',
            outlineColour: '#81CEFF',
            textHeight: 25,
            shadowBlur: 2,
            shadow: '#81CEFF',
            shadowOffset: [1,1],
            tooltip: 'div',
            tooltipDelay: 200,
            weight: true,
            //weightMode: 'both',
            freezeActive: true,
            reverse: true,
            depth: 0.8,
            shape: 'vcylinder',
            maxSpeed: 0.05
            */
    );

    return $options;
  }

  /**
   * Expose the options.
   *
   * TODO - the rest
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    ctools_include('dependent');
    $form['freezeActive'] = array(
      '#type' => 'checkbox',
      '#title' => t('freezeActive'),
      '#description' => t('stops animation when an item is hovered over'),
      '#default_value' => !empty($this->options['freezeActive']),
    );
  }
}