<?php

/**
  * Implementation of hook_views_plugin().
  */
function views_tagcanvas_views_plugins() {
  return array(
    'style' => array(
      'views_tagcanvas' => array(
        'title' => t('Views cloud - tagcanvas library'),
        'theme' => 'views_view_tagcanvas',
        'theme path' => drupal_get_path('module', 'views_tagcanvas') . '/includes',
        'help' => t('Provide an animated cloud view of the items.'),
        'handler' => 'views_tagcanvas_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}


