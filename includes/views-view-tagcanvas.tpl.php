
<div id="<?php print $views_tagcanvas_id ?>-wrapper"  class="views-tagcanvas-wrapper clear-block">
  <canvas width="1200" height="800"  id="<?php print $views_tagcanvas_id ?>">
    <p>Anything in here will be replaced on browsers that support the canvas element</p>
  </canvas>
  </div>
  <div id="<?php print $views_tagcanvas_id ?>-tags">
    <ul>
    <?php foreach ($rows as $row): ?>
      <li class="views-tagcanvas-item">
        <div class="views-tagcanvas-item-inner">
          <?php print $row ?>
        </div>
      </li>
    <?php endforeach; ?>
    </ul>
  </div>
</div>