/* Trigger the tagcanvas to apply to each view listed in the drupal.settings */

(function ($) {
  var pageLoaded = false;

  Drupal.behaviors.views_fluidgrid = {
    attach: function (context) {
      $('.views-tagcanvas-wrapper:not(.views-tagcanvas-processed)', context).each(function (index) {
        var $this = $(this),
        wrapper_id = $(this).attr('id'),
        // chop off the wrapper-prefix
        id = wrapper_id.substring(0, wrapper_id.length-8)
        settings = Drupal.settings.views_tagcanvas[id];
        try {
          // params are 
          // the 'canvas' element id
          // id of the wrapper that contains the tags
          // additional options.
          TagCanvas.Start(id, id + '-tags', settings);
        } catch(e) {
          // Something went wrong, hide the canvas container
          $('#' + id).hide();
          console.log("Failed to init tagcanvas on " + id + " " + id + "-tags");
        }
      }) // each view
    } // attach
  } // views_fluidgrid behavior
})(jQuery);